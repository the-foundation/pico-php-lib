<?php

function firstCharacters($str,$count=2) {
	return mb_substr($str, 0, $count, "UTF-8");
 }

function lastCharacters($str,$count=2) {
	return mb_substr($str, strlen($str)-$count , $count, "UTF-8");
 }

function obfuscateStringEnd($str,$count=2) {
	return firstCharacters($str,$count).str_repeat('*',strlen($str)-$count);

}

function obfuscateStringStart($str,$count=2) {
	return firstCharacters($str,$count).str_repeat('*',strlen($str)-$count);

}

function obfuscateMiddle($str,$count=2) {
        if(strlen($str) < ($count*2)) {
            $count=strlen($str)/2;
           }
////print "\n BGN ";
////echo mb_substr($str, 0, $count, "UTF-8");
////print "\n MID";
////echo preg_replace('/[^ ,\.]/',"*",mb_substr($str, $count, strlen($str)-$count-$count , "UTF-8"));
////print "\n END";
////echo mb_substr($str, strlen($str)-$count , $count, "UTF-8");
          // start with real chars            //middle obfusc except space,.                                                             //end w clear string
           return mb_substr($str, 0, $count, "UTF-8").preg_replace('/[^ ,\.]/',"*",mb_substr($str, $count, strlen($str)-$count-$count , "UTF-8")).mb_substr($str, strlen($str)-$count , $count, "UTF-8");

 }

function removeAllQuotes($string) {


	return  str_replace("'", "", str_replace('"', "", $string));
}

function removeSingleQuotes($string) {


	return  str_replace("'", "",  $string);
}

function removeDoubleQuotes($string) {


	return  str_replace('"', "", $string);
}


function domainOfMailAdress($addr) {
	return substr(strrchr($addr, "@"), 1);
 }

function userOfMailAddress($addr) {
	return strstr($addr, '@', true);
}
function webElem($src) {
	if(php_sapi_name() != "cli") { echo $src; };
}

if (!isset(Settings::$logdir)) {
	$logdir="/var/www/syncglogs/";
	}


function timeStamp() {
  return date("Y-m-d_H.i.s");
}
/**
 * Returns a valid email address or false
 * @param $email
 * @return mixed
 */
function trimEmail($email){
  $email = trim(strtolower($email));
  return filter_var(replace_uml($email), FILTER_VALIDATE_EMAIL);
}

function replace_uml($string){
  $search = array("Ä", "Ö", "Ü", "ä", "ö", "ü","ß");
  $replace = array("Ae", "Oe", "Ue", "ae", "oe", "ue","ss");
  return str_replace($search, $replace, $string);
}

function padString($string, $headline = true){
  $char = ($headline) ? '#' : ' ';
  return str_pad($string, 42, $char,STR_PAD_RIGHT) . '#';
}


/**
 * Obfuscates Email-Addresses for log-output showing only first two letters of name, last letter of
 * domain and top level domain.
 * @param $email
 * @return string
 */
function obfuscateEmailAddress($email){
  $email_split = explode("@", $email);
  $email_split[0] = substr($email_split[0], 0, 2);
  $extension = pathinfo($email_split[1], PATHINFO_EXTENSION);
  $email_split[1] = substr($email_split[1], -strlen($extension)-1, 1) . $extension;
  $email = implode("***@***",$email_split);
  return $email;
}


function verifyInternalToken(string $identifier,string $matchToken) {
	if(file_exists("/tmp/".$identifier."_token") && file_get_contents("/tmp/".$identifier."_token") == $matchToken ) {
		return true;
		} else { return false; }
}

function setInternalToken(string $identifier) {
  $internal_token="TOK_".md5(uniqid("id_").$identifier);
	file_put_contents( "/tmp/".$identifier."_token",$internal_token);
	return $internal_token;
}

function base_url() {
	return ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'];
}
function baseURL()  { return base_url(); }


function thisURL() {
	return ( isset($_SERVER['HTTPS']) && $_SERVER['HTTPS']=='on' ? 'https' : 'http' ) . '://' .  $_SERVER['HTTP_HOST'].$_SERVER['PHP_SELF'];
}


function selfURL() {
	return thisURL();
}

function Redis_AllKeys($redis) {
	$allKeys = $redis->keys('*');
}

function verifyMailAddressViaDNS($email) {
 $mailDomain = substr(strrchr($email, "@"), 1);
 if(empty($mailDomain)) {
 return false;
 }
 if (checkdnsrr($mailDomain, "MX") || checkdnsrr($mailDomain, "A") || checkdnsrr($mailDomain, "AAAA")) {
           return true;
 } else {  return false; }
}


?>
